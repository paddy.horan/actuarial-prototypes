from dataclasses import dataclass
from pathlib import Path
from typing import Dict, Tuple

import numpy as np
import pandas as pd

from data_loaders import load_disabled_life_mortality_rates, find_input_file, load_claim_recovery_rates
from enumerations import SitusOfCare
from general import (
    policy_month_to_claim_inforce_month,
    claim_start_condition,
    claim_end_condition,
    age_at_claim_inception,
    npv,
    claim_year_to_calendar_claim_year,
    policy_month_to_policy_year,
)
from input import PolicyCharacteristics, VALUATION_YEAR, InflationOptionType, InterestRates


@dataclass
class CostOfCare:
    data: Dict[str, float]

    def coc_inf_ratio_at_claim(self, claim_year):
        # TODO: Not implemented as constant and impl not clear...
        return self.data["coc_inf_ratio_at_claim"]

    def coc_inf_ratio_after_claim(self, claim_year):
        # TODO: Not implemented as constant and impl not clear...
        return self.data["coc_inf_ratio_after_claim"]

    def average_monthly_factor(self, claim_year):
        # TODO: Not implemented as constant and impl not clear...
        return self.data["average_monthly_factor"]

    def extended_benefit_months(self, claim_year):
        # TODO: Not implemented as constant and impl not clear...
        return self.data["extended_benefit_months"]


COC = CostOfCare(
    data=dict(
        coc_inf_ratio_at_claim=1.0,
        coc_inf_ratio_after_claim=1.0,
        average_monthly_factor=0.8,
        extended_benefit_months=30,
    )
)


def create_utilization_grid(
    valuation_year: int,
    projection_steps: int,
    # TODO: utilization rate can only be constant
    utilization_rate: float,
) -> np.ndarray:
    """
    Sets up the multi-dimensional array as a `np.ndarray`.

    Args:
        valuation_year: the valuation year at the beginning of the projection
        projection_steps: the number of steps to project
        utilization_rate: the utilization rate to use
    """
    total_yearly_steps = projection_steps + 1
    total_monthly_steps = total_yearly_steps * 12
    data = np.zeros((total_monthly_steps, total_yearly_steps, len(SitusOfCare))) * np.NAN
    for policy_duration_in_months in range(1, total_monthly_steps + 1):
        for claim_incurred_year in range(1, total_yearly_steps + 1):
            if claim_start_condition(
                valuation_year=valuation_year, policy_month=policy_duration_in_months, claim_year=claim_incurred_year
            ):
                data[policy_duration_in_months - 1, claim_incurred_year - 1, :] = utilization_rate
    return data


def create_claim_continuance_grid(
    bases_folder: Path, disabled_life_mortality_shock=1.0, recoveries_shock=1.0
) -> Tuple[np.ndarray, int]:
    """
    Creates a grid of claim continuance rates.

    This grid is 3 dimensional:
     - axis=0 is claim duration in months
     - axis=1 is the age of the claimant when the claim is incurred (from 18 to 120)
     - axis=2 is the situs of care

    Args:
        bases_folder: the location of the root of the bases folder
        disabled_life_mortality_shock: a scalar shock to apply to mortality
        recoveries_shock: a scalar shock to apply to recoveries

    Returns:
        a grid of claim continuance rates and the minimum incurred age
    """

    # Load durational factors
    durational_factors_location = find_input_file(bases_folder, "durational_factors_by_claim_month.csv", None)
    durational_factors = pd.read_csv(durational_factors_location, index_col=0)
    disabled_life_durational_factors = durational_factors["disabled_life_mortality"].values
    recovery_durational_factors = durational_factors["recoveries"].values

    # Load mortality and recovery rates
    disabled_mortality, min_incurred_age_m = load_disabled_life_mortality_rates(bases_folder, None)
    recovery_rates, min_incurred_age_r = load_claim_recovery_rates(bases_folder, None)
    assert min_incurred_age_r == min_incurred_age_m

    # Apply durational factors
    mortality_rates = disabled_mortality * disabled_life_durational_factors[:, np.newaxis, np.newaxis]
    recovery_rates = recovery_rates * recovery_durational_factors[:, np.newaxis, np.newaxis]

    # Shock rates
    shocked_mortality_rates = mortality_rates * disabled_life_mortality_shock
    shocked_recovery_rates = recovery_rates * recoveries_shock

    # Roll rates forward for vectorized application
    rolled_mortality_rates = np.roll(shocked_mortality_rates, 1, axis=0)
    rolled_recovery_rates = np.roll(shocked_recovery_rates, 1, axis=0)

    survival_rates = np.clip(np.ones_like(mortality_rates) - rolled_mortality_rates - rolled_recovery_rates, 0, 1)

    claim_continuance = np.ones(mortality_rates.shape, dtype=np.float64)
    for i in range(1, len(claim_continuance)):
        claim_continuance[i, :, :] = claim_continuance[i - 1, :, :] * survival_rates[i, :, :]

    return claim_continuance, min_incurred_age_m


def create_conditional_claim_inforce_grid(
    valuation_year: int,
    projection_steps: int,
    issue_age: int,
    claim_continuance_grid: np.ndarray,
    min_incurred_age: int,
) -> np.ndarray:
    """
    Creates a grid of expected claims inforce, **conditioned** on the policyholder incurring a claim in the during the
    claim incurred year.

    This grid is 3 dimensional:
     - axis=0 is policy duration in months
     - axis=1 is claim incurred year
     - axis=2 is the situs of care

    The grid contains NaN's where it is impossible to be inforce, i.e. where policy duration is less than claim
    incurred year.

    Args:
        valuation_year: the year that represents time 0 in our projection
        projection_steps: the number of steps to project in years
        issue_age: the age of the policyholder at issue
        claim_continuance_grid: a grid of claim continuance rates
        min_incurred_age: the minimum age that claim can be incurred in `claim_continuance_grid`

    Returns:
        a grid of claim inforce probabilities
    """

    total_yearly_steps = projection_steps + 1
    total_monthly_steps = total_yearly_steps * 12
    rates = np.zeros((total_monthly_steps, total_yearly_steps, len(SitusOfCare))) * np.NAN
    for claim_year in range(1, total_yearly_steps + 1):
        calendar_claim_year = claim_year_to_calendar_claim_year(valuation_year=valuation_year, claim_year=claim_year)
        ebm = COC.extended_benefit_months(claim_year=claim_year)
        for policy_month in range(1, total_monthly_steps + 1):
            starting_condition = claim_start_condition(
                valuation_year=valuation_year, policy_month=policy_month, claim_year=claim_year
            )
            ending_condition = claim_end_condition(
                valuation_year=VALUATION_YEAR, max_claim_length=ebm, claim_year=claim_year, policy_month=policy_month,
            )
            if starting_condition and ending_condition:
                claim_duration = policy_month_to_claim_inforce_month(
                    valuation_year=VALUATION_YEAR, policy_month=policy_month, calendar_claim_year=calendar_claim_year,
                )

                # TODO: Solution to max age...
                age = age_at_claim_inception(issue_age=issue_age, claim_inception_year=claim_year, max_age=120)
                continuance_rate = claim_continuance_grid[claim_duration - 1, age - min_incurred_age, :]
                rates[policy_month - 1, claim_year - 1, :] = continuance_rate[:]
    return rates


def create_claim_inforce_grid(conditional_claim_inforce_grid: np.ndarray, disabled_claims: np.ndarray) -> np.ndarray:
    """
    Creates a grid of expected claims inforce.

    This grid is 3 dimensional:
     - axis=0 is policy duration in months
     - axis=1 is claim incurred year
     - axis=2 is the situs of care

    The grid contains NaN's where it is impossible to be inforce, i.e. where policy duration is less than claim
    incurred year.

    Args:
        conditional_claim_inforce_grid: the claim inforce probabilities, **conditioned** on the policyholder incurring
        a claim in the during the claim incurred year
        disabled_claims: The expected number of claimants by claim incurred year

    Returns:
        a grid of claim inforce probabilities
    """
    return conditional_claim_inforce_grid * disabled_claims[np.newaxis, :, :]


@dataclass
class ProjectedBenefits:
    data: np.ndarray

    @classmethod
    def new(
        cls,
        valuation_year: int,
        projection_steps: int,
        utilization_rate: float,
        monthly_max_benefit: float,
        claimants_inforce,
        monthly_deductible: float,
        coinsurance_percentage: float,
    ):
        utilization_rates = create_utilization_grid(
            valuation_year=valuation_year, projection_steps=projection_steps, utilization_rate=utilization_rate
        )

        monthly_benefit_amount = monthly_max_benefit - monthly_deductible
        if PolicyCharacteristics.INFLATION_OPTION_TYPE is InflationOptionType.NONE:
            inflation_adjustment = 1.0
        else:
            # TODO: implement inflation...
            raise NotImplemented("Need to look at this.")
        # TODO: implement Cost of Care...
        cost_of_care_adjustment = 1.0

        benefit = monthly_benefit_amount * inflation_adjustment * cost_of_care_adjustment * coinsurance_percentage
        projected_benefits = claimants_inforce * utilization_rates * benefit
        set_to_zero = np.logical_and(np.isnan(claimants_inforce), np.logical_not(np.isnan(utilization_rates)))
        projected_benefits = np.where(set_to_zero, 0.0, projected_benefits)
        return cls(data=projected_benefits)

    def reserve_by_claim_year_and_disabled_type(self):
        monthly_valuation_interest_rate = np.power(1 + InterestRates.VALUATION_INTEREST_RATE, 1 / 12) - 1
        reserve = np.ones(self.data.shape[1:])
        for claim_year in range(1, len(reserve) + 1):
            for disabled_state in SitusOfCare:
                values = self.data[claim_year * 12 :, claim_year - 1, disabled_state]
                res = npv(monthly_valuation_interest_rate, values) / (1 + monthly_valuation_interest_rate)
                reserve[claim_year - 1, disabled_state] = res
        return reserve

    def sum_of_claims(self):
        return np.sum(np.where(np.isnan(self.data), 0.0, self.data), axis=0)

    def reserve_by_claim_year(self):
        return np.sum(self.reserve_by_claim_year_and_disabled_type(), axis=1)

    def benefits_by_policy_year(self):
        data_with_no_nans = np.where(np.isnan(self.data), 0.0, self.data)
        all_benefits = np.sum(np.sum(data_with_no_nans, axis=2), axis=1)
        policy_years = [
            policy_month_to_policy_year(valuation_year=VALUATION_YEAR, policy_month=i) for i in range(len(self.data))
        ]

        benefits_by_policy_year = pd.Series(all_benefits, index=policy_years)
        return benefits_by_policy_year.groupby(level=0).sum()

    def projected_benefit(self, policy_month, claim_year, disabled_state):
        return self.data[policy_month - 1, claim_year - 1, disabled_state]


# PROJECTED_BENEFITS = ProjectedBenefits.new(
#     valuation_year=VALUATION_YEAR,
#     projection_steps=PROJECTION_STEPS,
#     utilization_rate=0.8,
#     claimants_inforce=PROJECTED_CLAIMANTS,
#     monthly_max_benefit=PolicyCharacteristics.INITIAL_MONTHLY_MAXIMUM,
#     monthly_deductible=PolicyCharacteristics.DEDUCTIBLE,
#     coinsurance_percentage = PolicyCharacteristics.COINSURANCE_PERCENTAGE
# )
#
# for (test_policy_month, test_claim_incurred_year, test_disabled_state), expected_val in [
#     ((7, 1, SitusOfCare.NursingHome), 0.1643120752),
#     ((12, 1, SitusOfCare.NursingHome), 0.130179574497193),
#     ((13, 1, SitusOfCare.NursingHome), 0.126482532202695),
#     ((36, 1, SitusOfCare.NursingHome), 0.0838613239615902),
#     ((37, 1, SitusOfCare.NursingHome), 0.0),
# ]:
#     my_val = PROJECTED_BENEFITS.data[test_policy_month - 1, test_claim_incurred_year - 1, test_disabled_state]
#     assert (
#             round(my_val - expected_val, 10) == 0
#     ), f"Mismatch at {test_policy_month}, {test_claim_incurred_year}, {test_disabled_state}.  {my_val} vs {expected_val}"
#
# for (test_incurred_claim_year, test_disabled_state, expected_val) in [(1, SitusOfCare.NursingHome, 2.36423859284723)]:
#     my_val = PROJECTED_BENEFITS.reserve_by_claim_year_and_disabled_type()[
#         test_incurred_claim_year - 1, test_disabled_state
#     ]
#     assert (
#             round(my_val - expected_val, 10) == 0
#     ), f"Mismatch at {test_claim_incurred_year}, {test_disabled_state}.  {my_val - expected_val}"
#
# for (test_incurred_claim_year, expected_val) in [(1, 17.5037232171792)]:
#     my_val = PROJECTED_BENEFITS.reserve_by_claim_year()[test_incurred_claim_year - 1]
#     assert round(my_val - expected_val, 10) == 0, f"Mismatch at {test_claim_incurred_year}.  {my_val} vs {expected_val}"
#
# overall_diff = PROJECTED_BENEFITS.reserve_by_claim_year().sum() - 11288.0194151022
# # assert round(overall_diff, 10) == 0, overall_diff
#
# if __name__ == "__main__":
#     # print(coc)
#     # print(UTILIZATION.nursing_home_utilization_rate(7, 1))
#     # print(CLAIM_INCIDENCE_AND_CONTINUANCE)
#     test = PROJECTED_BENEFITS.reserve_by_claim_year()
#     # print(test)
#     print("All checks passing.")
