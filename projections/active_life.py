"""
Contains objects and functions for projecting active lives.
"""
from dataclasses import dataclass
from typing import Dict

import numpy as np
import pandas as pd

from data_loaders import load_active_life_lapse_rates, load_active_life_mortality_rates, load_incidence_rates
from enumerations import SitusOfCare


@dataclass
class SinglePeriodInforce:
    """
    Tracks inforce counts etc. for a single period.
    """

    attained_age: int
    lives_inforce_boy: float
    active_deaths: float
    active_lapses: float
    active_lives_boy: float
    disabled_claims: np.ndarray

    def __eq__(self, other) -> bool:
        # The built in impl will cause issues with float precision.
        for att in [
            "attained_age",
            "lives_inforce_boy",
            "active_deaths",
            "active_lapses",
            "active_lives_boy",
            "nursing_home_claims",
            "assisted_living_claims",
            "home_care_claims",
        ]:
            self_value = getattr(self, att)
            other_value = getattr(other, att)
            if round(self_value - other_value, 10):
                # print(f"SinglePeriodInforce.{att} does not match. ({self_value} vs {other_value})")
                return False
        return True

    @property
    def lives_inforce_eoy(self) -> float:
        """
        The number of lives inforce at the end of the year.
        """
        return self.lives_inforce_boy - self.active_deaths - self.active_lapses

    @property
    def total_ltc_claims(self) -> float:
        """
        The total number of LTC claims across all situs of care in the year.
        """
        return float(np.sum(self.disabled_claims))

    @property
    def nursing_home_claims(self) -> float:
        """
        The number of nursing home LTC claims in the year.
        """
        return self.disabled_claims[SitusOfCare.NursingHome]

    @property
    def assisted_living_claims(self) -> float:
        """
        The number of assisted living LTC claims in the year.
        """
        return self.disabled_claims[SitusOfCare.AssistedLiving]

    @property
    def home_care_claims(self) -> float:
        """
        The number of home care LTC claims in the year.
        """
        return self.disabled_claims[SitusOfCare.HomeCare]

    @property
    def active_lives_eoy(self) -> float:
        """
        The number of active lives inforce at the end of the year.
        Returns:

        """
        return self.active_lives_boy - sum([self.active_deaths, self.active_lapses, self.total_ltc_claims])

    def roll_forward(
        self, mortality_rate: float, lapse_rate: float, disabled_incidence_rates: np.ndarray
    ) -> "SinglePeriodInforce":
        """
        Rolls the inforce forward 1 period.

        Args:
            mortality_rate: the mortality rate to apply
            lapse_rate: the lapse rate to apply
            disabled_incidence_rates: the claim incidence rates to apply

        Returns:
            the rolled-forward inforce
        """
        new_age = self.attained_age + 1
        return SinglePeriodInforce(
            attained_age=new_age,
            lives_inforce_boy=self.lives_inforce_eoy,
            active_deaths=mortality_rate * self.active_lives_eoy,
            active_lapses=lapse_rate * self.active_lives_eoy,
            active_lives_boy=self.active_lives_eoy,
            disabled_claims=disabled_incidence_rates * self.active_lives_eoy,
        )

    def to_dict(self) -> Dict[str, float]:
        """
        Converts this instance to a dictionary.
        """
        return dict(
            attained_age=self.attained_age,
            lives_inforce_boy=self.lives_inforce_boy,
            active_deaths=self.active_deaths,
            active_lapses=self.active_lapses,
            lives_inforce_eoy=self.lives_inforce_eoy,
            active_lives_boy=self.active_lives_boy,
            nursing_home_claims=self.nursing_home_claims,
            assisted_living_claims=self.assisted_living_claims,
            home_care_claims=self.home_care_claims,
            total_claims=self.total_ltc_claims,
            active_lives_eoy=self.active_lives_eoy,
        )


@dataclass
class ActiveLifeProjection:
    """
    Contains the projection of active lives.
    """

    data: Dict[int, SinglePeriodInforce]

    def __getitem__(self, item) -> SinglePeriodInforce:
        return self.data[item]

    def to_df(self) -> pd.DataFrame:
        """
        Converts the projection to a `pd.DataFrame`.
        """
        return pd.DataFrame.from_records([i.to_dict() for i in self.data.values()], index=self.data.keys())

    @property
    def disabled_claims(self) -> np.ndarray:
        """
        The projection of disabled claims.  Axis 1 varies by situs of care.
        """
        return self.to_df().loc[:, ["nursing_home_claims", "assisted_living_claims", "home_care_claims"]].values


def project_active_lives(initial_calendar_year, initial_age, projection_steps, basis) -> ActiveLifeProjection:
    """
    Projects active lives.

    The disabled lives are not projected, only the active lives.

    Args:
        initial_calendar_year: the year of the projection
        initial_age: the age of the policy holder
        projection_steps: the number of steps to project
        basis: the assumption basis to use for the projection

    Returns:
        the projected active lives (with decrements)
    """
    active_life_lapse_rates = load_active_life_lapse_rates(basis=basis, issue_age=initial_age)
    active_life_mortality_rates = load_active_life_mortality_rates(basis=basis, issue_age=initial_age)
    disabled_claim_incidence_rates = load_incidence_rates(basis=basis, issue_age=initial_age)
    active_lives = {
        initial_calendar_year: SinglePeriodInforce(
            attained_age=initial_age,
            lives_inforce_boy=1.0,
            active_deaths=active_life_mortality_rates.loc[initial_age, "blended"],
            active_lapses=active_life_lapse_rates.loc[initial_age, "blended"],
            active_lives_boy=1.0,
            disabled_claims=disabled_claim_incidence_rates.loc[initial_age].values,
        )
    }
    for initial_age in range(initial_age + 1, initial_age + 1 + projection_steps):
        mortality_rate = active_life_mortality_rates.loc[initial_age, "blended"]
        lapse_rate = active_life_lapse_rates.loc[initial_age, "blended"]
        incidence_rates = disabled_claim_incidence_rates.loc[initial_age].values
        projection_year, inforce = list(active_lives.items())[-1]
        active_lives[projection_year + 1] = inforce.roll_forward(
            mortality_rate=mortality_rate, lapse_rate=lapse_rate, disabled_incidence_rates=incidence_rates
        )

    projection = ActiveLifeProjection(data=active_lives)

    return projection
