from typing import Optional

import numpy as np


def attained_age_to_calendar_policy_year(valuation_year: int, issue_age: int, attained_age: int) -> int:
    return attained_age - issue_age + valuation_year


def policy_month_to_policy_year(valuation_year: int, policy_month: int) -> int:
    return valuation_year + int(policy_month / 12) + 1


def policy_month_to_calendar_policy_year(valuation_year: int, policy_month: int) -> int:
    return valuation_year + int((policy_month - 1) / 12.0) + 1


def policy_month_to_attained_age(valuation_year: int, policy_month: int, issue_age: int) -> int:
    policy_year = policy_month_to_calendar_policy_year(valuation_year, policy_month)
    return issue_age + policy_year - valuation_year - 1


def claim_year_to_calendar_claim_year(valuation_year: int, claim_year: int) -> int:
    return valuation_year + claim_year


def policy_month_to_claim_inforce_month(
    valuation_year: int, policy_month: int, claim_year: Optional[int] = None, calendar_claim_year: Optional[int] = None
):
    if sum(list(map(lambda x: x is None, [claim_year, calendar_claim_year]))) != 1:
        raise ValueError("You must pass 'claim_year' or 'calendar_claim_year' but not both.")
    if claim_year:
        calendar_claim_year = claim_year_to_calendar_claim_year(valuation_year, claim_year)
    claim_start_month = 12 * (calendar_claim_year - valuation_year - 1) + 6
    return policy_month - claim_start_month


def claim_start_condition(valuation_year, policy_month, claim_year):
    calendar_policy_year = policy_month_to_calendar_policy_year(
        valuation_year=valuation_year, policy_month=policy_month
    )
    calendar_claim_year = claim_year_to_calendar_claim_year(valuation_year=valuation_year, claim_year=claim_year)
    return any(
        [
            calendar_policy_year > calendar_claim_year,
            calendar_policy_year == calendar_claim_year and (policy_month % 12 > 6 or not policy_month % 12),
        ]
    )


def claim_end_condition(valuation_year, max_claim_length, claim_year, policy_month):
    calendar_claim_year = claim_year_to_calendar_claim_year(valuation_year=valuation_year, claim_year=claim_year)
    last_active_claim_month = max_claim_length + 6 + 12 * (calendar_claim_year - valuation_year - 1)
    ending_condition = policy_month <= last_active_claim_month
    return ending_condition


def age_at_claim_inception(issue_age: int, claim_inception_year: int, max_age: int) -> int:
    # TODO: EXISTING_CLAIM_INFORCE_PERIOD,
    return min([sum([issue_age, claim_inception_year, -1]), max_age])


def npv(rate, values):
    values = np.asarray(values)
    return (values / (1 + rate) ** np.arange(0, len(values))).sum(axis=0)
