from enum import IntEnum


class SitusOfCare(IntEnum):
    NursingHome = 0
    AssistedLiving = 1
    HomeCare = 2
