import plotly.graph_objs as go
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from main import main

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

ticker_name_dic = [18, 60]
options = []
for ticker in ticker_name_dic:
    options.append({"label": "IssueAge={}".format(ticker), "value": ticker})


def create_sidebar():
    SIDEBAR_STYLE = {
        "position": "fixed",
        "top": 0,
        "left": 0,
        "bottom": 0,
        "width": "30rem",
        "padding": "2rem 1rem",
        "background-color": "#f8f9fa",
    }

    controls = dbc.Card(
        [
            dbc.FormGroup(
                [
                    dcc.Dropdown(id="issue_age", options=options, value=60),
                    dbc.Button(id="submit-button", n_clicks=0, children="Submit", color="primary", block=True),
                ]
            )
        ],
    )

    sidebar = html.Div([html.H2("Bain Model", className="display-4"), html.Hr(), controls], style=SIDEBAR_STYLE,)
    return sidebar


CONTENT_STYLE = {
    "margin-left": "32rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}
content = html.Div(
    [
        html.H1("Projection", style={"textAlign": "center"}),
        html.Hr(),
        dbc.Row(
            [
                dbc.Col(dcc.Graph(id="Income"), width={"size": 8, "offset": 2}),
                dbc.Col(dcc.Graph(id="Outgo"), width={"size": 8, "offset": 2}),
            ]
        ),
    ],
    style=CONTENT_STYLE,
)


@app.callback(Output("Income", "figure"), [Input("submit-button", "n_clicks")], [State("issue_age", "value")])
def update_graph(n_clicks, issue_age):
    data = main(issue_age=issue_age)
    fig = go.Figure(data=[go.Bar(name="Premium Income", x=data.index, y=data["premium_income"]),])
    fig.layout.update(barmode="stack", title="Projected Income (Premium)")
    return fig


@app.callback(Output("Outgo", "figure"), [Input("submit-button", "n_clicks")], [State("issue_age", "value")])
def update_graph(n_clicks, issue_age):
    data = main(issue_age=issue_age)
    # graph_data = [{'x': data.index, 'y': data['distributable_earnings']}]
    # fig = {
    #     'data': graph_data,
    #     'layout': {'title': "TEST"}
    # }
    fig = go.Figure(
        data=[
            go.Bar(name="Commission", x=data.index, y=data["commissions"]),
            go.Bar(name="Maintenance Expenses", x=data.index, y=data["maintenance_expenses"]),
            go.Bar(name="NeverStop Fees", x=data.index, y=data["neverstop_fees"]),
            go.Bar(name="LTC Benefits", x=data.index, y=data["ltc_benefits"]),
        ]
    )
    fig.layout.update(barmode="stack", title="Projected Outgo")
    return fig


app.layout = html.Div([create_sidebar(), content])


if __name__ == "__main__":
    app.run_server(port="8083")
