# from enumerations import SitusOfCare
# from input import VALUATION_YEAR, PROJECTION_STEPS, PolicyCharacteristics
# from projections.disabled_life import PROJECTED_CLAIM_CONTINUANCE, ProjectedBenefits
#
# for (test_pm, test_cy, test_ds), expected_val in [
#     ((7, 1, SitusOfCare.NursingHome), 1.0),
#     ((8, 1, SitusOfCare.NursingHome), 0.91352863),
#     ((18, 1, SitusOfCare.NursingHome), 0.68609308779672),
#     ((19, 1, SitusOfCare.NursingHome), 0.671973486713795),
#     ((36, 1, SitusOfCare.NursingHome), 0.510378338655358),
#     ((537, 45, SitusOfCare.NursingHome), 0.764071465097626),
# ]:
#     clm_cont = PROJECTED_CLAIM_CONTINUANCE.rate(test_pm, test_cy, test_ds)
#     assert (
#             round(expected_val - clm_cont, 10) == 0
#     ), f"Mismatch at {test_pm}, {test_cy}, {test_ds}. {clm_cont} vs {expected_val}"
#
# DISABLED_LIFE_RESERVE_PROJECTION = ProjectedBenefits.new(
#     valuation_year=VALUATION_YEAR,
#     projection_steps=PROJECTION_STEPS,
#     utilization_rate=0.8,
#     claimants_inforce=PROJECTED_CLAIM_CONTINUANCE,
#     monthly_max_benefit=PolicyCharacteristics.INITIAL_MONTHLY_MAXIMUM,
#     monthly_deductible=PolicyCharacteristics.DEDUCTIBLE,
#     coinsurance_percentage=PolicyCharacteristics.COINSURANCE_PERCENTAGE
# )
#
# for (test_pm, test_cy, test_ds), expected_val in [
#     ((535, 45, SitusOfCare.NursingHome), 2068.64),
#     ((536, 45, SitusOfCare.NursingHome), 1743.2276562652),
#     ((537, 45, SitusOfCare.NursingHome), 1580.58879555955),
#     ((538, 45, SitusOfCare.NursingHome), 1433.12374127934),
#     ((564, 45, SitusOfCare.NursingHome), 79.1317421071309),
# ]:
#     test_reserve = DISABLED_LIFE_RESERVE_PROJECTION.projected_benefit(test_pm, test_cy, test_ds)
#     assert (
#             round(expected_val - test_reserve, 10) == 0
#     ), f"Mismatch at {test_pm}, {test_cy}, {test_ds}. {clm_cont} vs {expected_val}"
#
# for ((test_cy, test_ds), expected_val) in [
#     ((45, SitusOfCare.NursingHome), 9593.60186924656),
# ]:
#     my_val = DISABLED_LIFE_RESERVE_PROJECTION.reserve_by_claim_year_and_disabled_type()[test_cy - 1, test_ds]
#     check = round(my_val - expected_val, 10)
#     assert check == 0, f"Mismatch at {test_cy}, {test_ds}. {check}"
#
# for (test_claim_year, expected_val) in [
#     (1, 87576.108260552),
#     (2, 87553.7485340557),
#     (10, 87340.8902522969),
#     (20, 85870.4966947913),
#     (30, 92575.4887168259),
#     (40, 51508.8876292552),
#     (42, 41710.3108510039),
#     (44, 33188.8147507706),
#     (45, 28780.8056077397),
#     (50, 4607.83109489945),
#     (59, 4607.83109489945),
#     (60, 0.0),
# ]:
#     my_val = DISABLED_LIFE_RESERVE_PROJECTION.reserve_by_claim_year()[test_claim_year - 1]
#     check = round(my_val - expected_val, 3)
#     assert check == 0, f"Mismatch at {test_claim_year}. {check}"
#
# if __name__ == "__main__":
#     test = DISABLED_LIFE_RESERVE_PROJECTION.reserve_by_claim_year_and_disabled_type()[7 - 1, 0]
#     # print(test)
