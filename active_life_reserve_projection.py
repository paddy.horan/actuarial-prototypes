import numpy as np
import pandas as pd

from data_loaders import load_incidence_rates, find_input_file
from input import (
    PolicyCharacteristics,
    PROJECTION_STEPS,
    VALUATION_YEAR,
    InterestRates,
    BASES_FOLDER,
    SensitivityFactors,
)
from inputs.active_reserve_assumptions import ActiveLifeReservingLapseAssumption, ActiveLifeReservingMortalityAssumption
from projections.disabled_life import (
    ProjectedBenefits,
    create_conditional_claim_inforce_grid,
    create_claim_continuance_grid,
)


def project_active_life_reserve(issue_age):
    active_reserving_lapse_assumption = ActiveLifeReservingLapseAssumption(
        valuation_year=VALUATION_YEAR,
        issue_age=issue_age,
        path_to_csv_input=find_input_file(BASES_FOLDER, "active_lapse.csv", "reserving"),
    )

    active_reserving_mortality_assumption = ActiveLifeReservingMortalityAssumption(
        path_to_csv_input=find_input_file(BASES_FOLDER, "active_mortality.csv", "reserving"),
        percentage_male=PolicyCharacteristics.PERCENT_MALE,
    )

    policy_year_start = VALUATION_YEAR + 1
    attained_age_start = issue_age

    attained_age = list(range(attained_age_start, attained_age_start + PROJECTION_STEPS + 1))
    discount_factors = np.power(
        (1 + InterestRates.VALUATION_INTEREST_RATE), np.arange(0, -1 * (PROJECTION_STEPS + 1), -1)
    )
    projection = pd.DataFrame.from_dict(dict(attained_age=attained_age, discount_factors=discount_factors,))
    projection.index = list(range(policy_year_start, policy_year_start + PROJECTION_STEPS + 1))

    # Add Lives at the beginning of the year
    lives_boy = [1.0]
    for (calendar_policy_year, age) in zip(projection.index[1:], attained_age[1:]):
        lapse_rate = active_reserving_lapse_assumption.lapse_rate(attained_age=age - 1)
        mortality_rate = active_reserving_mortality_assumption.mortality_rate(attained_age=age - 1)
        lives_boy.append(lives_boy[-1] * (1 - lapse_rate - mortality_rate))  # TODO: Should be multiplied...
    projection["lives_boy"] = lives_boy

    # Add the annual LTC amount
    incidence_rates = load_incidence_rates("NOT SURE", issue_age=issue_age).loc[attained_age, :].values
    claim_continuance_grid, min_incurred_age = create_claim_continuance_grid(BASES_FOLDER)
    conditional_claim_inforce_grid = create_conditional_claim_inforce_grid(
        valuation_year=VALUATION_YEAR,
        projection_steps=PROJECTION_STEPS,
        issue_age=issue_age,
        claim_continuance_grid=claim_continuance_grid,
        min_incurred_age=min_incurred_age,
    )

    disabled_life_reserve_projection = ProjectedBenefits.new(
        valuation_year=VALUATION_YEAR,
        projection_steps=PROJECTION_STEPS,
        utilization_rate=0.8,
        claimants_inforce=conditional_claim_inforce_grid,
        monthly_max_benefit=PolicyCharacteristics.INITIAL_MONTHLY_MAXIMUM,
        monthly_deductible=PolicyCharacteristics.DEDUCTIBLE,
        coinsurance_percentage=PolicyCharacteristics.COINSURANCE_PERCENTAGE,
    )
    reserves = disabled_life_reserve_projection.sum_of_claims()
    annual_ltc_amount = np.sum(incidence_rates * reserves, axis=1)
    projection["ltc_amount"] = annual_ltc_amount

    pv_of_future_premiums = []
    pv_of_future_benefits = []
    for row in range(len(projection)):
        lives = projection["lives_boy"].iloc[row:]
        scaled_lives = lives / lives.iloc[0]
        discount_factors = projection["discount_factors"].iloc[row:]
        scaled_discount_factors = discount_factors / discount_factors.iloc[0]
        pv_premiums = np.sum(scaled_lives * scaled_discount_factors * PolicyCharacteristics.PREMIUM)
        pv_of_future_premiums.append(pv_premiums)

        ltc = projection["ltc_amount"].iloc[row:]
        pv_benefits = np.sum(scaled_lives * scaled_discount_factors * ltc)
        pv_of_future_benefits.append(pv_benefits)

    projection["pv_of_future_premiums"] = pv_of_future_premiums
    projection["pv_of_future_benefits"] = pv_of_future_benefits

    # TODO: Not sure about this...
    ftp_exp_allow = projection["pv_of_future_benefits"].iloc[1] / projection["pv_of_future_premiums"].iloc[1]

    stat_reserve = []
    for idx, age in enumerate(projection["attained_age"]):
        # TODO: What is the stopping condition for the projection?
        if age >= 115 or idx + 1 >= len(projection):
            stat_reserve.append(0)
        else:
            benefit = projection["pv_of_future_benefits"].iloc[idx + 1]
            premium = projection["pv_of_future_premiums"].iloc[idx + 1]
            stat_reserve.append(benefit - (premium * ftp_exp_allow))
    projection["statutory_reserve"] = stat_reserve

    return projection


if __name__ == "__main__":
    project_active_life_reserve()
