"""
Main execution script.
"""

import numpy as np

from active_life_reserve_projection import project_active_life_reserve
from input import (
    PolicyCharacteristics,
    VALUATION_YEAR,
    Commissions,
    Expenses,
    NeverStopFees,
    PROJECTION_STEPS,
    SensitivityFactors,
    InterestRates,
    TargetSurplus,
    FEDERAL_INCOME_TAX,
    BASES_FOLDER,
)
from projections.active_life import project_active_lives
from projections.disabled_life import (
    ProjectedBenefits,
    create_claim_continuance_grid,
    create_conditional_claim_inforce_grid,
    create_claim_inforce_grid,
)


def main(issue_age):
    projection = project_active_lives(
        initial_calendar_year=VALUATION_YEAR + 1,
        initial_age=issue_age,
        projection_steps=PROJECTION_STEPS,
        basis="projection",
    ).to_df()
    projection["premium_income"] = projection["lives_inforce_boy"] * PolicyCharacteristics.PREMIUM
    disabled_claims = project_active_lives(
        initial_calendar_year=VALUATION_YEAR,
        initial_age=issue_age,
        projection_steps=PROJECTION_STEPS,
        basis="projection",
    ).disabled_claims
    claim_continuance_grid, min_incurred_age = create_claim_continuance_grid(BASES_FOLDER)
    conditional_claim_inforce_grid = create_conditional_claim_inforce_grid(
        valuation_year=VALUATION_YEAR,
        projection_steps=PROJECTION_STEPS,
        issue_age=60,
        claim_continuance_grid=claim_continuance_grid,
        min_incurred_age=min_incurred_age,
    )
    claim_inforce_grid = create_claim_inforce_grid(
        conditional_claim_inforce_grid=conditional_claim_inforce_grid, disabled_claims=disabled_claims
    )

    projected_disabled_benefits = ProjectedBenefits.new(
        valuation_year=VALUATION_YEAR,
        projection_steps=PROJECTION_STEPS,
        utilization_rate=0.8,
        claimants_inforce=claim_inforce_grid,
        monthly_max_benefit=PolicyCharacteristics.INITIAL_MONTHLY_MAXIMUM,
        monthly_deductible=PolicyCharacteristics.DEDUCTIBLE,
        coinsurance_percentage=PolicyCharacteristics.COINSURANCE_PERCENTAGE,
    )
    ltc_benefits = projected_disabled_benefits.benefits_by_policy_year()
    projection["ltc_benefits"] = ltc_benefits

    # Add commission
    is_first_year = projection.index == VALUATION_YEAR + 1
    first_year_commission = is_first_year * Commissions.FIRST_YEAR * projection["premium_income"]
    renewal_commission_payable = np.logical_and(
        np.logical_not(is_first_year), projection.index <= VALUATION_YEAR + Commissions.RENEWAL_TERM + 1
    )
    renewal_commission = renewal_commission_payable * Commissions.RENEWAL * projection["premium_income"]
    projection["commissions"] = first_year_commission + renewal_commission

    # Add Acquisition Expenses
    if SensitivityFactors.DISTRIBUTION.Traditional:
        acq_exp = Expenses.ACQUISITION_PERCENT_OF_PREMIUM * projection["premium_income"] * is_first_year
        acq_exp += Expenses.ACQUISITION_PER_POLICY
    else:
        acq_exp = np.zeros(len(projection))
    projection["acquisition_expenses"] = acq_exp

    # Add maintenance expenses (includes premium tax)
    expense_inflation = np.power(1 + Expenses.MAINTENANCE_EXPENSE_INFLATION, np.arange(len(projection)))
    per_policy_maintenance = Expenses.MAINTENANCE_PER_POLICY * projection.lives_inforce_boy * expense_inflation
    claim_maintenance = ltc_benefits * Expenses.MAINTENANCE_PERCENT_OF_CLAIM
    premium_maintenance = Expenses.MAINTENANCE_PERCENT_OF_PREMIUM * projection.premium_income
    projection["maintenance_expenses"] = per_policy_maintenance + claim_maintenance + premium_maintenance

    # NeverStop
    if SensitivityFactors.DISTRIBUTION.NeverStop:
        acquisition_expenses = is_first_year * sum(
            [
                projection.premium_income * NeverStopFees.ACQUISITION_PERCENT_OF_PREMIUM,
                NeverStopFees.ACQUISITION_PER_POLICY,
            ]
        )
        inf = np.power((1 + NeverStopFees.RECURRING_PER_POLICY_INFLATION), np.arange(0, (PROJECTION_STEPS + 1)))
        per_policy_fees = projection.lives_inforce_boy * inf * NeverStopFees.RECURRING_PER_POLICY
        per_claim_fees = ltc_benefits * NeverStopFees.RECURRING_PERCENT_OF_CLAIM
        per_premium_fees = projection.premium_income * NeverStopFees.RECURRING_PERCENT_OF_PREMIUM
        projection["neverstop_fees"] = acquisition_expenses + per_policy_fees + per_claim_fees + per_premium_fees
    else:
        projection["neverstop_fees"] = np.zeros(len(projection))

    outgo = projection.loc[
        :, ["commissions", "acquisition_expenses", "maintenance_expenses", "neverstop_fees", "ltc_benefits"]
    ].sum(axis=1)
    projection["net_cashflow"] = projection.premium_income - outgo

    earned_rate = InterestRates.ASSET_EARNED_RATE + SensitivityFactors.ASSET_EARNED_RATE_ADDITIVE
    return_on_capital = InterestRates.RETURN_ON_CAPITAL + SensitivityFactors.RETURN_ON_CAPITAL_ADDITIVE
    # TODO: I'm not sure why we "rollback" and use the reserve at the next period
    active_life_reserve_projection = project_active_life_reserve(issue_age=issue_age)
    active_reserve = (
        np.roll(active_life_reserve_projection["statutory_reserve"].values, -1) * projection.active_lives_eoy
    )
    disabled_reserve = projected_disabled_benefits.reserve_by_claim_year().T
    total_reserve = active_reserve + disabled_reserve
    surplus_due_to_premium = projection.premium_income * TargetSurplus.PERCENT_OF_PREMIUM
    surplus_due_to_claims = ltc_benefits * TargetSurplus.PERCENT_OF_CLAIMS
    surplus = surplus_due_to_premium + surplus_due_to_claims
    projection["investment_income"] = sum(
        [(projection.net_cashflow + total_reserve) * earned_rate, return_on_capital * surplus]
    )

    change_in_reserve = total_reserve.diff(1)
    change_in_reserve.iloc[0] = total_reserve.iloc[0]
    projection["change_in_reserve"] = change_in_reserve
    projection["pre_tax_profit"] = projection.net_cashflow + projection.investment_income - change_in_reserve

    projection["federal_income_tax"] = (projection.pre_tax_profit + 0.0719 * change_in_reserve) * FEDERAL_INCOME_TAX
    projection["after_tax_profit"] = projection.pre_tax_profit - projection.federal_income_tax

    change_in_surplus = surplus.diff(1)
    change_in_surplus.iloc[0] = surplus.iloc[0]
    projection["change_in_surplus"] = change_in_surplus
    projection["distributable_earnings"] = projection.after_tax_profit - change_in_surplus

    return projection


if __name__ == "__main__":
    # ages = [18, 60, 65, 70, 75]
    ages = [18, 60]
    for age in ages:
        main_projection = main(issue_age=age)
        # print(main_projection)
