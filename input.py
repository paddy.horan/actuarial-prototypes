from enum import IntEnum
from pathlib import Path

BASES_FOLDER = Path(__file__).parent / "bases"

VALUATION_YEAR = 2021
PROJECTION_STEPS = 59
FEDERAL_INCOME_TAX = 0.21


class InterestRates:
    VALUATION_INTEREST_RATE = 0.035
    ASSET_EARNED_RATE = 0.0325
    RETURN_ON_CAPITAL = 0.0325


class InflationOptionType(IntEnum):
    NONE = 0
    COMPOUND = 1
    SIMPLE = 2


class UtilizationType(IntEnum):
    Other = 0
    Full = 1


class DistributionMethod(IntEnum):
    Traditional = 0
    NeverStop = 1


class PolicyCharacteristics:
    # ISSUE_AGE = 60
    PERCENT_MALE = 1.0
    PERCENT_PREFERRED = 1.0
    INFLATION_OPTION_TYPE = InflationOptionType.NONE
    BENEFIT_INFLATION_RATE = 0.0
    INITIAL_DAILY_MAXIMUM = 122.62
    INITIAL_MONTHLY_MAXIMUM = round(INITIAL_DAILY_MAXIMUM * 365.25 / 12, 2)
    INITIAL_BENEFIT_PERIOD_M = 24
    EXISTING_CLAIM_INFORCE_PERIOD = 0
    COINSURANCE_PERCENTAGE = 0.8
    DEDUCTIBLE = 500
    PREMIUM = 1600  # TODO: Need to add premium rates...


# TODO: Add Carrier...
class Commissions:
    FIRST_YEAR = 0.5
    RENEWAL = 0.0
    RENEWAL_TERM = 0


class TargetSurplus:
    PERCENT_OF_PREMIUM = 0.3
    PERCENT_OF_CLAIMS = 0.9


class Utilization:
    ANNUAL_COC_INFLATION = 0.035
    INFLATION_EFFECT_DAMPLING = 0.7


# TODO: This is calculated...
# ANNUAL_PREMIUM_PER_POLICY = None


class SensitivityFactors:
    DISABLED_LIFE_MORTALITY = 1.0
    RECOVERIES = 1.0
    INCIDENCE_FACTOR = 1.0
    ACTIVE_LIFE_MORTALITY = 1.0
    LAPSE = 1.0
    UTILIZATION = UtilizationType.Full
    ASSET_EARNED_RATE_ADDITIVE = 0.0025
    RETURN_ON_CAPITAL_ADDITIVE = 0.0025
    PREMIUM = 1.0
    DISTRIBUTION = DistributionMethod.NeverStop


class Expenses:
    ACQUISITION_PER_POLICY = {18: 546.00, 60: 664.8, 65: 824.4, 70: 991.8, 75: 1023.00}
    ACQUISITION_PERCENT_OF_PREMIUM = 0.77
    MAINTENANCE_PER_POLICY = 124
    MAINTENANCE_PERCENT_OF_CLAIM = 0.027
    MAINTENANCE_PERCENT_OF_PREMIUM = 0.02  # Includes premium tax
    MAINTENANCE_EXPENSE_INFLATION = 0.015  # per-policy


class NeverStopFees:
    ACQUISITION_PER_POLICY = 400.0  # UW costs + margin (300 + 100)
    ACQUISITION_PERCENT_OF_PREMIUM = 0.4
    RECURRING_PER_POLICY = 250  # Wellness program fees
    RECURRING_PER_POLICY_INFLATION = 0.02
    RECURRING_PERCENT_OF_CLAIM = 0.0
    RECURRING_PERCENT_OF_PREMIUM = 0.00
