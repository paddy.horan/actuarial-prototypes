from pathlib import Path
from typing import Optional, Tuple

import numpy as np
import pandas as pd

from enumerations import SitusOfCare
from input import PolicyCharacteristics, SensitivityFactors, BASES_FOLDER


def find_input_file(bases_folder: Path, file_name: str, basis: Optional[str]):
    """
    Finds an input file's location falling back to the root non-basis specific location if the basis specific version
    does not exist.

    Args:
        bases_folder: the root of the bases folder
        file_name: the name of the file to find
        basis: an optional basis to look under

    Returns:
        the location of the file requested
    """
    if basis is not None:
        basis_specific_path = bases_folder / basis / file_name
        if basis_specific_path.exists():
            return basis_specific_path
    non_basis_path = bases_folder / file_name
    if not non_basis_path.exists():
        raise ValueError(f"Could not file '{file_name}' in bases folder.")

    return non_basis_path


def blend_rates(rate_table, male_proportion):
    """
    Blends the make and female rates.

    `rate_table` should have `male` and `female` attributes.

    Args:
        rate_table: base table with male and female rates
        male_proportion: the proportion of males in the population

    Returns:
        a blended unisex rate(s)
    """
    return (rate_table.male * male_proportion) + (rate_table.female * (1 - male_proportion))


def load_rate_table(location_of_base_rate_table, location_of_durational_factor_table, durational_factor, issue_age):
    """
    Loads a base rate table and applies durational factors.

    Args:
        location_of_base_rate_table: the location of the base rate table
        location_of_durational_factor_table: the location of the duration factor table
        durational_factor: the column of the durational factor to apply

    Returns:
        the loaded rate table with a new "blended" column with durational factors applied
    """
    base_rate_table = pd.read_csv(location_of_base_rate_table, index_col=0)
    blended = blend_rates(base_rate_table, male_proportion=PolicyCharacteristics.PERCENT_MALE)
    durational_factors = pd.read_csv(location_of_durational_factor_table, index_col=0)
    table_size = len(durational_factors)
    attained_age = base_rate_table.index.values
    policy_year = attained_age - issue_age + 1
    policy_year = np.where(policy_year < 1, 1, policy_year)
    policy_year = np.where(policy_year > table_size, table_size, policy_year)
    durational_factor = durational_factors.reindex(list(policy_year))[durational_factor]
    base_rate_table["blended"] = (blended.values * durational_factor.values) * SensitivityFactors.LAPSE
    return base_rate_table


def load_active_life_lapse_rates(basis, issue_age):
    """
    Loads the active life lapse rates.

    Args:
        basis: the basis to load

    Returns:
        the loaded active life lapse rates
    """
    location_of_base_rate_table = find_input_file(BASES_FOLDER, "active_lapse.csv", basis)
    location_of_durational_factor_table = find_input_file(BASES_FOLDER, "durational_factors_by_policy_year.csv", basis)
    loaded_table = load_rate_table(
        location_of_base_rate_table=location_of_base_rate_table,
        location_of_durational_factor_table=location_of_durational_factor_table,
        durational_factor="active_lapse",
        issue_age=issue_age,
    )
    return loaded_table


def load_active_life_mortality_rates(basis, issue_age):
    """
    Loads the active life mortality rates.

    Args:
        basis: the basis to load

    Returns:
        the loaded active life mortality rates
    """
    location_of_base_rate_table = find_input_file(BASES_FOLDER, "active_mortality.csv", basis)
    location_of_durational_factor_table = find_input_file(BASES_FOLDER, "durational_factors_by_policy_year.csv", basis)
    loaded_table = load_rate_table(
        location_of_base_rate_table=location_of_base_rate_table,
        location_of_durational_factor_table=location_of_durational_factor_table,
        durational_factor="active_mortality",
        issue_age=issue_age,
    )
    return loaded_table


def load_incidence_rates(basis, issue_age) -> pd.DataFrame:
    """
    Loads the incidence rates.

    After loading we blend the male and female base incidence rates, apply situs of care proportions and apply
    durational factors.

    Args:
        basis: the basis to load incidence rates for

    Returns:
        the loaded and prepared rates
    """
    location_of_incidence_rate_table = find_input_file(BASES_FOLDER, "incidence_rates.csv", basis)
    location_of_durational_factors = find_input_file(BASES_FOLDER, "durational_factors_by_policy_year.csv", basis)
    incidence_table = pd.read_csv(location_of_incidence_rate_table, index_col=0)
    durational_factors = pd.read_csv(location_of_durational_factors, index_col=0)
    blended_base_rates = blend_rates(incidence_table, male_proportion=PolicyCharacteristics.PERCENT_MALE)
    nursing_home_proportion = incidence_table.nursing_home_proportion
    assisted_living_proportion = incidence_table.assisted_living_proportion
    home_care_proportion = 1.0 - nursing_home_proportion - assisted_living_proportion

    # Looks up the durational factors
    table_size = len(durational_factors)
    attained_age = incidence_table.index.values
    policy_year = attained_age - issue_age + 1
    policy_year = np.where(policy_year < 1, 1, policy_year)
    policy_year = np.where(policy_year > table_size, table_size, policy_year)
    durational_factors = durational_factors.reindex(list(policy_year))
    base_incidence = blended_base_rates.values

    nursing_home_durational_factors = durational_factors["nursing_home_incidence"].values
    nursing_home = base_incidence * nursing_home_proportion * nursing_home_durational_factors

    assisted_living_durational_factors = durational_factors["assisted_living_incidence"].values
    assisted_living = base_incidence * assisted_living_proportion * assisted_living_durational_factors

    home_care_durational_factors = durational_factors["home_care_incidence"].values
    home_care = base_incidence * home_care_proportion * home_care_durational_factors

    df = (
        pd.DataFrame.from_dict(
            {"nursing_home": nursing_home, "assisted_living": assisted_living, "home_care": home_care}
        )
        * SensitivityFactors.INCIDENCE_FACTOR
    )
    df.index = range(1, len(df) + 1)

    return df


def load_table_by_claim_duration_and_claim_incurred_age(bases_folder, table_name, basis):
    """
    Helper function to load the mortality tables.
    """
    table_location = find_input_file(bases_folder, table_name, basis)
    table = pd.read_csv(table_location, index_col=0)
    table.index = range(1, len(table) + 1)
    table.index.name = "claim_duration"
    table.columns.name = "claim_incurred_age"
    return table


def get_min_age(*tables: pd.DataFrame) -> int:
    """
    Checks that the minimum incurred age is the same in each table and returns it.

    Args:
        *tables: the tables to check

    Returns:
        the minimum incurred age
    """
    min_incurred_age = list({t.columns.tolist()[0] for t in tables})
    assert len(min_incurred_age) == 1
    min_incurred_age = int(min_incurred_age[0])
    return min_incurred_age


def load_disabled_life_mortality_rates(bases_folder: Path, basis) -> Tuple[np.ndarray, int]:
    """
    Loads the disabled life mortality rates.

    Args:
        bases_folder: the location of the bases folder
        basis: the basis to load

    Returns:
        the loaded disabled life mortality rates and the minimum incurred age
    """

    mortality_nh = load_table_by_claim_duration_and_claim_incurred_age(bases_folder, "disabled_mortality_nh.csv", basis)
    mortality_al = load_table_by_claim_duration_and_claim_incurred_age(bases_folder, "disabled_mortality_al.csv", basis)
    mortality_hc = load_table_by_claim_duration_and_claim_incurred_age(bases_folder, "disabled_mortality_hc.csv", basis)
    min_incurred_age = get_min_age(mortality_nh, mortality_hc, mortality_al)

    disabled_life_mortality = np.zeros((len(mortality_nh), len(mortality_nh.columns), len(SitusOfCare)))
    disabled_life_mortality[:, :, SitusOfCare.NursingHome] = mortality_nh.values
    disabled_life_mortality[:, :, SitusOfCare.AssistedLiving] = mortality_al.values
    disabled_life_mortality[:, :, SitusOfCare.HomeCare] = mortality_hc.values

    return disabled_life_mortality, min_incurred_age


def load_claim_recovery_rates(bases_folder: Path, basis) -> Tuple[np.ndarray, int]:
    """
    Loads the disabled life claim recovery rates.

    Args:
        bases_folder: the location of the bases folder
        basis: the basis to load

    Returns:
        the loaded disabled life mortality rates and the minimum incurred age
    """

    recoveries_nh = load_table_by_claim_duration_and_claim_incurred_age(bases_folder, "recoveries_nh.csv", basis)
    recoveries_al = load_table_by_claim_duration_and_claim_incurred_age(bases_folder, "recoveries_al.csv", basis)
    # TODO: Remove 'fillna'
    recoveries_hc = load_table_by_claim_duration_and_claim_incurred_age(
        bases_folder, "recoveries_hc.csv", basis
    ).fillna(0)
    min_incurred_age = get_min_age(recoveries_nh, recoveries_al, recoveries_hc)

    recoveries = np.zeros((len(recoveries_nh), len(recoveries_nh.columns), len(SitusOfCare)))
    recoveries[:, :, SitusOfCare.NursingHome] = recoveries_nh.values
    recoveries[:, :, SitusOfCare.AssistedLiving] = recoveries_al.values
    recoveries[:, :, SitusOfCare.HomeCare] = recoveries_hc.values

    return recoveries, min_incurred_age
