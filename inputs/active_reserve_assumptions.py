import pandas as pd

from data_loaders import load_active_life_lapse_rates
from general import attained_age_to_calendar_policy_year
from inputs import RAW_INPUT_FOLDER
from input import PolicyCharacteristics


class ActiveLifeReservingLapseAssumption:
    def __init__(self, valuation_year, issue_age, path_to_csv_input):

        self.valuation_year = valuation_year
        self.issue_age = issue_age
        self.naic_assumptions = pd.read_csv(path_to_csv_input, index_col=0)
        self.projection_basis_lapse_rates = load_active_life_lapse_rates(basis="projection", issue_age=issue_age)

    def lapse_rate(self, attained_age: int) -> float:
        calendar_policy_year = (
            attained_age_to_calendar_policy_year(
                valuation_year=self.valuation_year, issue_age=self.issue_age, attained_age=attained_age
            )
            + 1
        )
        best_estimate = self.projection_basis_lapse_rates.loc[attained_age, "blended"]
        naic_wx_scalar = self.naic_assumptions.loc[calendar_policy_year, "naic_scalar"]
        naic_wx_max = self.naic_assumptions.loc[calendar_policy_year, "naic_max"]
        return min([naic_wx_max, best_estimate * naic_wx_scalar])


class ActiveLifeReservingMortalityAssumption:
    def __init__(self, path_to_csv_input, percentage_male: float):
        self._1994_gam_ltc_valuation_mortality = pd.read_csv(path_to_csv_input, index_col=0)
        self.percentage_male = percentage_male

    def mortality_rate(self, attained_age: int) -> float:
        capped_attained_age = min([attained_age, list(self._1994_gam_ltc_valuation_mortality.index)[-1]])
        male_mortality = self._1994_gam_ltc_valuation_mortality.loc[capped_attained_age, "male"]
        female_mortality = self._1994_gam_ltc_valuation_mortality.loc[capped_attained_age, "female"]
        return (male_mortality * self.percentage_male) + (female_mortality * (1 - self.percentage_male))


# ACTIVE_RESERVING_LAPSE_ASSUMPTION = ActiveLifeReservingLapseAssumption(
#     valuation_year=input.VALUATION_YEAR,
#     issue_age=PolicyCharacteristics.ISSUE_AGE,
#     path_to_csv_input=BASES_FOLDER / "active_lapse.csv",
# )
#
#
# ACTIVE_RESERVING_MORTALITY_ASSUMPTION = ActiveLifeReservingMortalityAssumption(
#     path_to_csv_input=RAW_INPUT_FOLDER / "active_mortality.csv",
#     percentage_male=PolicyCharacteristics.PERCENT_MALE,
# )
#
# if __name__ == "__main__":
#
#     rate = ACTIVE_RESERVING_LAPSE_ASSUMPTION.lapse_rate(60)
#     print(rate)
#
#     rate = ACTIVE_RESERVING_MORTALITY_ASSUMPTION.mortality_rate(60)
#     print(rate)
