from pathlib import Path

import pandas as pd
from input import BASES_FOLDER

RAW_INPUT_FOLDER = Path(__file__).parent.parent / "raw_inputs"


def load_durational_factors_by_policy_year():
    df = pd.read_csv(BASES_FOLDER / "durational_factors_by_policy_year.csv", index_col=0)
    return df
