"""
Tests helpers for loading data.
"""
import pytest

from data_loaders import load_active_life_lapse_rates, load_active_life_mortality_rates, load_incidence_rates
from enumerations import SitusOfCare


@pytest.fixture(scope="session")
def active_lapse_rates():
    return load_active_life_lapse_rates("projection", 60)


@pytest.fixture(scope="session")
def active_mortality_rates():
    return load_active_life_mortality_rates("projection", 60)


@pytest.fixture(scope="session")
def incidence_rates():
    return load_incidence_rates(None, 60)


@pytest.fixture(scope="session", params=list(iter(SitusOfCare)))
def situs_of_care(request):
    return request.param


@pytest.fixture(scope="session", params=[1, 6, 57, 59, 60, 61, 119, 120])
def age(request):
    return request.param


def test_loaded_active_lapse_rates(active_lapse_rates, age):
    lapse_rate = active_lapse_rates.loc[age, "blended"]
    expected_lapse_rate = {1: 0.064, 6: 0.064, 57: 0.064, 59: 0.064, 60: 0.064, 61: 0.056, 119: 0.008, 120: 0.008}[age]

    assert lapse_rate == expected_lapse_rate


def test_loaded_active_mortality_rates(active_mortality_rates, age):
    mortality_rate = active_mortality_rates.loc[age, "blended"]
    expected_mortality_rate = {
        1: 0.0000166,
        6: 0.0000162,
        57: 0.000403,
        59: 0.0004864,
        60: 0.0005356,
        61: 0.0007365,
        119: 0.26,
        120: 1.0,
    }[age]

    assert round(mortality_rate, 10) == expected_mortality_rate


def test_loaded_incidence_rates(incidence_rates, age, situs_of_care):
    incidence_rate = incidence_rates.loc[age, :].values[situs_of_care]
    expected_incidence_rate = {
        (1, SitusOfCare.NursingHome): 0.00003822,
        (1, SitusOfCare.AssistedLiving): 0.00006468,
        (1, SitusOfCare.HomeCare): 0.0001911,
        (6, SitusOfCare.NursingHome): 0.00003822,
        (6, SitusOfCare.AssistedLiving): 0.00006468,
        (6, SitusOfCare.HomeCare): 0.0001911,
        (57, SitusOfCare.NursingHome): 0.00003822,
        (57, SitusOfCare.AssistedLiving): 0.00006468,
        (57, SitusOfCare.HomeCare): 0.0001911,
        (59, SitusOfCare.NursingHome): 0.00006227,
        (59, SitusOfCare.AssistedLiving): 0.00010538,
        (59, SitusOfCare.HomeCare): 0.00031135,
        (60, SitusOfCare.NursingHome): 0.00007943,
        (60, SitusOfCare.AssistedLiving): 0.00013442,
        (60, SitusOfCare.HomeCare): 0.00039715,
        (61, SitusOfCare.NursingHome): 0.00010127,
        (61, SitusOfCare.AssistedLiving): 0.00017138,
        (61, SitusOfCare.HomeCare): 0.00050635,
        (119, SitusOfCare.NursingHome): 0.29651995,
        (119, SitusOfCare.AssistedLiving): 0.0348847,
        (119, SitusOfCare.HomeCare): 0.01744235,
        (120, SitusOfCare.NursingHome): 0.29651995,
        (120, SitusOfCare.AssistedLiving): 0.0348847,
        (120, SitusOfCare.HomeCare): 0.01744235,
    }[(age, situs_of_care)]

    assert round(incidence_rate, 10) == expected_incidence_rate
