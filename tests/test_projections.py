import numpy as np
import pytest

from input import VALUATION_YEAR, PolicyCharacteristics, PROJECTION_STEPS
from projections.active_life import project_active_lives, SinglePeriodInforce


@pytest.fixture(scope="session")
def active_life_projection():
    return project_active_lives(
        initial_calendar_year=VALUATION_YEAR + 1, initial_age=60, projection_steps=PROJECTION_STEPS, basis="projection",
    )


def test_2023(active_life_projection):
    assert active_life_projection[2023] == SinglePeriodInforce(
        61,
        0.935464400000000,
        0.000688519529100,
        0.052351790400000,
        0.934853400000000,
        np.array([0.000094672603818, 0.000160215175692, 0.000473363019090]),
    )


def test_2032(active_life_projection):
    assert active_life_projection[2032] == SinglePeriodInforce(
        70,
        0.7192019943929640,
        0.0035994508811225,
        0.0056113192604751,
        0.7014149075593840,
        np.array([0.0006652780115219, 0.0011258550964217, 0.0033263900576096,]),
    )


def test_2081(active_life_projection):
    assert active_life_projection[2081] == SinglePeriodInforce(
        119,
        0.38619558380358,
        0.00000000000663,
        0.00000000000020,
        0.00000000002548,
        np.array([0.00000000000756, 0.00000000000089, 0.00000000000044,]),
    )
