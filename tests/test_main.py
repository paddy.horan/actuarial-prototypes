import pytest

from main import main


@pytest.fixture(scope="session")
def main_projection():
    return main(60)


def test_distributable_earnings_2022(main_projection):
    assert round(main_projection.loc[2022, "distributable_earnings"] - -1473.77, 1) == 0


def test_distributable_earnings_2026(main_projection):
    assert round(main_projection.loc[2026, "distributable_earnings"] - 411.02, 1) == 0


def test_distributable_earnings_2050(main_projection):
    assert round(main_projection.loc[2050, "distributable_earnings"] - 429.48, 1) == 0


def test_distributable_earnings_2081(main_projection):
    assert round(main_projection.loc[2081, "distributable_earnings"] - 152.06, 1) == 0
