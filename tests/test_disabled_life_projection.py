# CLAIM_CONTINUANCE = ClaimContinuanceProbability.new(bases_folder=BASES_FOLDER)

# for (claim_year, claim_age, claim_type), expected_value in [
#     ((360, 18, 0), 0.0088755730),
#     ((360, 60, 0), 0.000430202927),
#     ((360, 18, 1), 0.0088755730),  # Same as nursing_home
#     ((360, 60, 1), 0.000430202927),  # Same as nursing_home
#     ((360, 18, 2), 0.0087932508232),
#     ((360, 60, 2), 0.0004265146393),
# ]:
#     my_val = CLAIM_CONTINUANCE.data[claim_year - 1, claim_age - 18, claim_type]
#     assert (
#         round(my_val - expected_value, 10) == 0
#     ), f"Mismatch at {claim_year}, {claim_age}, {claim_type}, {my_val} vs {expected_value}"
#
#
# if __name__ == "__main__":
#     print(CLAIM_CONTINUANCE)
