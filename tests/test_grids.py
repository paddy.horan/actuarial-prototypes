import numpy as np
import pytest

from enumerations import SitusOfCare
from input import BASES_FOLDER, VALUATION_YEAR, PROJECTION_STEPS
from projections.active_life import project_active_lives
from projections.disabled_life import (
    create_claim_continuance_grid,
    create_conditional_claim_inforce_grid,
    create_claim_inforce_grid,
)


@pytest.fixture(scope="session")
def claim_inforce_grid():
    disabled_claims = project_active_lives(
        initial_calendar_year=VALUATION_YEAR, initial_age=60, projection_steps=PROJECTION_STEPS, basis="projection",
    ).disabled_claims
    claim_continuance_grid, min_incurred_age = create_claim_continuance_grid(BASES_FOLDER)
    conditional_claim_inforce_grid = create_conditional_claim_inforce_grid(
        valuation_year=VALUATION_YEAR,
        projection_steps=PROJECTION_STEPS,
        issue_age=60,
        claim_continuance_grid=claim_continuance_grid,
        min_incurred_age=min_incurred_age,
    )
    return create_claim_inforce_grid(
        conditional_claim_inforce_grid=conditional_claim_inforce_grid, disabled_claims=disabled_claims
    )


@pytest.fixture(
    scope="session",
    params=[
        ((6, 1, SitusOfCare.NursingHome), np.NAN),
        ((7, 1, SitusOfCare.NursingHome), 0.00007943),
        ((36, 1, SitusOfCare.NursingHome), 0.0000405393514394),
        ((37, 1, SitusOfCare.NursingHome), np.NAN),
        ((30, 3, SitusOfCare.NursingHome), np.NAN),
        ((32, 3, SitusOfCare.NursingHome), 0.000104008690341061),
        ((61, 3, SitusOfCare.NursingHome), np.NAN),
        ((7, 1, SitusOfCare.AssistedLiving), 0.00013442),
        ((90, 8, SitusOfCare.AssistedLiving), np.NAN),
        ((91, 8, SitusOfCare.AssistedLiving), 0.0005431877506),
        ((120, 8, SitusOfCare.AssistedLiving), 0.000275806990676351),
        ((121, 8, SitusOfCare.AssistedLiving), np.NAN),
        ((642, 54, SitusOfCare.HomeCare), np.NAN),
        ((643, 54, SitusOfCare.HomeCare), 0.0000000001404878866),
        ((672, 54, SitusOfCare.HomeCare), 0.0),
        ((673, 54, SitusOfCare.HomeCare), np.NAN),
    ],
)
def case(request):
    return request.param


def test_claims_inforce(claim_inforce_grid, case):
    ((policy_month, claim_incurred_year, situs_of_care), expected_result) = case
    result = claim_inforce_grid[policy_month - 1, claim_incurred_year - 1, situs_of_care]
    if np.isnan(expected_result):
        assert np.isnan(result)
    else:
        assert round(result - expected_result, 10) == 0
